FROM debian
WORKDIR /go/src/gitlab.com/server-go-simple

ADD main .
RUN apt-get update
CMD ["./main"]
EXPOSE 80